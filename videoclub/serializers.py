from rest_framework import serializers
from rest_framework.renderers import JSONRenderer
from .models import Alquileres, Clientes, Dvds, Peliculas, Generos
from django.core.exceptions import ValidationError


class ClientesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clientes
        fields = (
            "id",
            "dni",
            "nombre",
            "apellido",
            "direccion",
            "numero_telefono",
            "correo",
        )
        
        read_only_field = (
            "created_at",
        )

  
    
class AlquileresSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alquileres
        fields = (
            "id",
            "cliente",
            "dvd",
            "nro_recibo",
            "precio",
            "fecha_alquiler",
            "fecha_devolucion",
            
        )
        read_only_field = ("created_at",)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        cliente = ClientesSerializer(instance.cliente).data
        dvd = DvdsSerializer(instance.dvd).data
        ret["cliente_dni"] = cliente["dni"]
        ret["cliente"] = cliente["nombre"] + " " + cliente["apellido"]
        ret["dvd"] = dvd["identificador_dvd"]
        return ret
    
class DvdsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dvds
        fields = ("id", "pelicula", "identificador_dvd", "disponible")
        read_only_field = ("created_at",)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        pelicula = PeliculasSerializer(instance.pelicula).data
        ret["pelicula"] = pelicula["titulo"]
        return ret


class PeliculasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Peliculas
        fields = ("id", "genero", "titulo", "director", "estreno")
        read_only_field = ("created_at",)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        genero = GenerosSerializer(instance.genero).data
        ret["genero"] = genero["descripcion"]
        return ret


class GenerosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Generos
        fields = ("id", "codigo", "descripcion")
        read_only_field = ("created_at",)
