# Generated by Django 4.1.7 on 2023-02-24 22:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videoclub', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dvds',
            name='identificador_dvd',
            field=models.CharField(max_length=150, unique=True),
        ),
    ]
