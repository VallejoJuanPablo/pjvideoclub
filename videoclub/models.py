from django.db import models

# Create your models here.
class Clientes(models.Model):
    dni = models.CharField(max_length=15,unique=True)
    nombre = models.CharField(max_length=150)
    apellido =  models.CharField(max_length=150)
    direccion =  models.CharField(max_length=255)
    numero_telefono = models.CharField(max_length=20)
    correo = models.CharField(max_length=100)
    created_at = models.DateField(auto_now_add=True)
    update_at = models.DateField(auto_now=True)
    
    def __str__(self):
        return self.apellido + " " + self.nombre

class Generos(models.Model):   
    codigo = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=100)
    created_at = models.DateField(auto_now_add=True)
    update_at = models.DateField(auto_now=True)
    
    def __str__(self):
        return self.descripcion


class Peliculas(models.Model):
    genero =  models.ForeignKey(Generos,null=True,on_delete=models.SET_NULL)
    titulo = models.CharField(max_length=50)
    director = models.CharField(max_length=100)
    estreno =  models.DateField()
    created_at = models.DateField(auto_now_add=True)
    update_at = models.DateField(auto_now=True)
    
    def __str__(self):
        return self.titulo
    
class Dvds(models.Model):
    pelicula =  models.ForeignKey(Peliculas,null=True,on_delete=models.SET_NULL)
    identificador_dvd = models.CharField(max_length=150,unique=True)
    disponible =  models.BooleanField()
    created_at = models.DateField(auto_now_add=True)
    update_at = models.DateField(auto_now=True)
    
    def __str__(self):
        return " -Codigo: " +self.identificador_dvd + " -Pelicula : " + self.pelicula.titulo
    
class Alquileres(models.Model):
    cliente = models.ForeignKey(Clientes,null=True,on_delete=models.SET_NULL)
    dvd = models.ForeignKey(Dvds,null=True,on_delete=models.SET_NULL)
    nro_recibo = models.IntegerField()
    fecha_alquiler = models.DateField(auto_now_add=True)
    fecha_devolucion = models.DateField(blank=True,null=True)
    precio = models.FloatField()
    created_at = models.DateField(auto_now_add=True)
    update_at = models.DateField(auto_now=True)
    
    def __str__(self):  
        return " -Nro Recibo: " + str(self.nro_recibo) + " -Pelicula: " + self.dvd.pelicula.titulo + " - Cliente: " + self.cliente.apellido + " " + self.cliente.nombre 