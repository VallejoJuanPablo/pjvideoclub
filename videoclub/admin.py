from django.contrib import admin
from .models import Alquileres,Clientes,Dvds,Peliculas,Generos
# Register your models here.

admin.site.register(Alquileres)
admin.site.register(Clientes)
admin.site.register(Dvds)
admin.site.register(Peliculas)
admin.site.register(Generos)
