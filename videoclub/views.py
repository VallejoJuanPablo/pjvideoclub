from rest_framework.views import APIView
from rest_framework import status
from django.http import JsonResponse
from datetime import datetime
from .models import Alquileres, Peliculas, Dvds, Clientes, Generos
from .serializers import (
    AlquileresSerializer,
    ClientesSerializer,
    DvdsSerializer,
    PeliculasSerializer,
    GenerosSerializer,
)

import json


class PeliculasApiView(APIView):
    def get(self, request):
        peliculas = Peliculas.objects.all()
        genero = self.request.query_params.get("genero", None)
        print(genero)
        if genero:
            peliculas = peliculas.filter(genero_id=genero)

        serializer = PeliculasSerializer(peliculas, many=True)

        if serializer.data:
            return JsonResponse(
                {"message": "Listado de peliculas", "data": serializer.data}, status=status.HTTP_200_OK
            )
        else:
            return JsonResponse(
                {"message": "Listado de peliculas vacia", "data": ""}, status=status.HTTP_403_FORBIDDEN
            )

    def post(self, request):
        solicitud = json.loads(request.body)
        serializer = PeliculasSerializer(data=solicitud)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(
                {"message": "Agregado", "data": serializer.data},
                status=status.HTTP_200_OK,
            )
        else:
            error = serializer.errors
            return JsonResponse(
                {"Error message": error, "data": ""}, status=status.HTTP_403_FORBIDDEN
            )

    def delete(self, request, id):
        try:
            pelicula = Peliculas.objects.get(id=id)
            pelicula.delete()
            return JsonResponse({"message": "Eliminado"}, status=status.HTTP_200_OK)
        except:
            return JsonResponse(
                {"message": "Error", "error": "Pelicula no encontrada"},
                status=status.HTTP_403_FORBIDDEN,
            )


class GenerosApiView(APIView):
    def get(self, request):
        genero = Generos.objects.all()
        serializer = GenerosSerializer(genero, many=True)
        if serializer.data:
            return JsonResponse(
                {"message": "Listado de géneros", "data": serializer.data},
                status=status.HTTP_200_OK,
            )
        else:
            return JsonResponse(
                {"message": "Lista de géneros vacia", "data": ""}, status=status.HTTP_403_FORBIDDEN
            )

    def delete(self, request, id):
        try:
            genero = Generos.objects.get(id=id)
            genero.delete()
            return JsonResponse({"message": "Eliminado"}, status=status.HTTP_200_OK)
        except:
            return JsonResponse(
                {"message": "Error", "error": "Genero no encontrado"},
                status=status.HTTP_403_FORBIDDEN,
            )


class DvdsApiView(APIView):
    def get(self, request):
        dvd = Dvds.objects.all()

        serializer = DvdsSerializer(dvd, many=True)

        if serializer.data:
            return JsonResponse(
                {"message": "Listado de Dvds", "data": serializer.data},
                status=status.HTTP_200_OK,
            )
        else:
            return JsonResponse(
                {"message": "Lista de Dvds vacia", "data": ""}, status=status.HTTP_403_FORBIDDEN
            )

    def post(self, request):
        solicitud = json.loads(request.body)
        serializer = DvdsSerializer(data=solicitud)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(
                {"message": "Agregado", "data": serializer.data}, status=status.HTTP_200_OK
            )
        else:
            error = serializer.errors
            return JsonResponse(
                {"Error message": error, "data": ""}, status=status.HTTP_403_FORBIDDEN
            )


class ClientesApiView(APIView):
    # agrego metodo get solo para que sea mas facil mirar los cambios de los otros endpoint
    def get(self, request):
        clientes = Clientes.objects.all()

        serializer = ClientesSerializer(clientes, many=True)

        if serializer.data:
            return JsonResponse(
                {"message": "Listado de Clientes", "data": serializer.data},
                status=status.HTTP_200_OK,
            )
        else:
            return JsonResponse(
                {"message": "Lista de Clientes vacia", "data": ""}, status=status.HTTP_403_FORBIDDEN
            )

    def post(self, request):
        solicitud = json.loads(request.body)
        serializer = ClientesSerializer(data=solicitud)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(
                {"message": "Ok", "data": serializer.data}, status=status.HTTP_200_OK
            )
        else:
            error = serializer.errors
            return JsonResponse(
                {"Error message": error, "data": ""}, status=status.HTTP_403_FORBIDDEN
            )

    def patch(self, request, id):
        try:
            cliente = Clientes.objects.get(id=id)
        except Clientes.DoesNotExist:
            return JsonResponse(
                {"message": "Error", "error": "Cliente no encontrado"},
                status=status.HTTP_404_NOT_FOUND,
            )

        request = json.loads(request.body)

        for (key, value) in request.items():
            if key in ["dni", "nombre", "apellido", "direccion"]:
                error = {key: "the field " + key + " cannot be updated."}
                return JsonResponse(
                    {"Error message": error, "data": ""},
                    status=status.HTTP_403_FORBIDDEN,
                )

        serializer = ClientesSerializer(cliente, data=request, partial=True)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(
                {"message": "Modificado", "data": serializer.data},
                status=status.HTTP_200_OK,
            )
        else:
            error = serializer.errors
            return JsonResponse(
                {"Error message": error, "data": ""}, status=status.HTTP_403_FORBIDDEN
            )


class AlquileresApiView(APIView):
    def get(self, request):
        alquiler = Alquileres.objects.all()
        dni = self.request.query_params.get("dni", None)
        fecha_alquiler = self.request.query_params.get("fecha_alquiler", None)

        if dni:
            alquiler = alquiler.filter(cliente_id__dni=dni)
            serializer = AlquileresSerializer(alquiler, many=True)
            if serializer.data:
                return JsonResponse({"message": "Listado de Alquileres -Dni: " + dni, "data": serializer.data},status=status.HTTP_200_OK,)
            else:
                return JsonResponse(
                {"message": "No existen alquileres con ese número de documento", "data": ""}, status=status.HTTP_403_FORBIDDEN
            )
        
        if fecha_alquiler:
            # El formato default es YYYY-MM-DD
            format = "%Y-%m-%d"
            try:
                res = bool(datetime.strptime(fecha_alquiler, format))
            except:
                error = {"fecha_alquiler": "the field fecha_alquiler must have format YYYY-MM-DD"}
                return JsonResponse({'message': error, 'data': ''}, status=status.HTTP_403_FORBIDDEN)
            
            alquiler = alquiler.filter(fecha_alquiler=fecha_alquiler)
            serializer = AlquileresSerializer(alquiler, many=True)
            if serializer.data:
                return JsonResponse({"message": "Listado de alquileres -Fecha: " + fecha_alquiler, "data": serializer.data},status=status.HTTP_200_OK,)
            else:   
                return JsonResponse(
                {"message": "No existen alquileres realizados en esa fecha", "data": ""}, status=status.HTTP_403_FORBIDDEN
            )
            
        alquiler = Alquileres.objects.all()
        serializer = AlquileresSerializer(alquiler, many=True)
        if serializer.data:
            return JsonResponse({"message": "Listado de Alquileres", "data": serializer.data},status=status.HTTP_200_OK,)
        else:
             return JsonResponse(
                {"message": "Error", "data": ""}, status=status.HTTP_403_FORBIDDEN
            )
             
    def post(self, request):
        solicitud = json.loads(request.body)
        dvds = Dvds.objects.all()
        dvd = dvds.filter(disponible=True,id=solicitud.get('dvd'))
        if dvd:
            dvd = Dvds.objects.get(id=solicitud.get('dvd'))
            serializer = DvdsSerializer(dvd, data= {'disponible': False}, partial=True)
            if serializer.is_valid():
                serializer.save()
            else:
                error = serializer.errors
                return JsonResponse({"Error message": error, "data": ""}, status=status.HTTP_403_FORBIDDEN )

            serializer = AlquileresSerializer(data=solicitud)
            if serializer.is_valid():
                serializer.save()
                return JsonResponse({"message": "Ok", "data": serializer.data}, status=status.HTTP_200_OK)
            else:
                error = serializer.errors
                return JsonResponse({"Error message": error, "data": ""}, status=status.HTTP_403_FORBIDDEN)
        else:
            return JsonResponse(
                 {"message": "Error", "error": "Dvd no disponible"}, status=status.HTTP_403_FORBIDDEN
            )

    def patch(self, request, id):
        try:
            alquiler = Alquileres.objects.get(id=id)
        except Alquileres.DoesNotExist:
            return JsonResponse(
                {"message": "Error", "error": "Alquiler no encontrado"},
                status=status.HTTP_404_NOT_FOUND,
            )

        request = json.loads(request.body)

        for (key, value) in request.items():
            if key in ["nro_recibo", "fecha_alquiler", "dvd", "cliente"]:
                error = {key: "the field " + key + " cannot be updated."}
                return JsonResponse(
                    {"Error message": error, "data": ""},
                    status=status.HTTP_403_FORBIDDEN,
                )

            serializer = DvdsSerializer(alquiler.dvd, data= {'disponible': True}, partial=True)
            if serializer.is_valid():
                serializer.save()
            else:
                error = serializer.errors
                return JsonResponse({"Error message": error, "data": ""}, status=status.HTTP_403_FORBIDDEN )


        serializer = AlquileresSerializer(alquiler, data=request, partial=True)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(
                {"message": "Ok", "data": serializer.data},
                status=status.HTTP_200_OK,
            )
        else:
            error = serializer.errors
            return JsonResponse(
                {"Error message": error, "data": ""}, status=status.HTTP_403_FORBIDDEN
            )
